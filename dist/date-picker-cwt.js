'use strict';
(function (module, _) {
    try {
        module = angular.module('cwt.date-picker');
    } catch (e) {
        module = angular.module('cwt.date-picker', []);
    }
    var directiveName = "cwtDatePicker";
    var theDirective = function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                datePickerModel: '=',
                hasError: '=?',
                placeholder: '@',
                datePickerOutputModel: '=?'
            },
            templateUrl: 'templates/cwt-date-picker.html',
            controllerAs: 'ctrl',
            bindToController: true,
            controller: ['$scope', function ($scope) {
                var ctrl = this
                ctrl.format = 'dd MMM yyyy';
                ctrl.opened = false;
                ctrl.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1,
                    initDate: null,
                    showWeeks: false
                };
                ctrl.altInputFormats = ['M!/d!/yyyy'];
                ctrl.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    ctrl.opened = !ctrl.opened;
                };
                var internalChange = false;
                $scope.$watch('ctrl.datePickerModel', function () {
                    var model = angular.copy(ctrl.datePickerModel);
                    if (!(model instanceof Date) && notEmpty(model)) {
                        if (typeof model === 'string') {
                            model = new Date(model.split('+')[0]);
                        }
                        else {
                            model = new Date(model);
                        }
                        if( model.getHours() != 0) {
                            model = model.addHours((model.getTimezoneOffset() / 60));
                        }
                        model.setHours(12, 0, 0, 0);
                        ctrl.displayModel = angular.copy(model);
                        model = model.getTime();
                    }
                    
                    if (notEmpty(model)) {
                        ctrl.displayModel = angular.copy(model);
                        if ((model instanceof Date)) {
                            if( model.getHours() != 0) {
                                model = model.addHours((model.getTimezoneOffset() / 60));
                            }
                            model.setHours(12, 0, 0, 0);
                            model = model.getTime();
                        }
                        
                    }
                    else {
                        ctrl.displayModel = model;
                    }
                    if (!internalChange) {
                        if (notEmpty(model)) {
                            if ((model instanceof Date)) {
                                if( model.getHours() != 0) {
                                    model = model.addHours((model.getTimezoneOffset() / 60));
                                }
                                model.setHours(12, 0, 0, 0);
                                model = model.getTime();
                            }
                        }
                        ctrl.datePickerOutputModel = model;
                    }
                    internalChange = true;

                }, true);

                $scope.$watch('ctrl.displayModel', function () {
                    if (!internalChange) {
                        if (ctrl.displayModel) {
                            var model = angular.copy(ctrl.displayModel)
                            if ((model instanceof Date)) {
                                if( model.getHours() != 0) {
                                    model = model.addHours((model.getTimezoneOffset() / 60));
                                }
                                model.setHours(12, 0, 0, 0);
                            }
                            ctrl.datePickerOutputModel = model.getTime();
                            ctrl.datePickerModel = ctrl.displayModel;
                            internalChange = true;
                        }
                        else {
                            ctrl.datePickerOutputModel = null;
                        }
                    } else {
                        internalChange = false;
                    }
                });

                Date.prototype.addHours = function (h) {
                    this.setHours(this.getHours() + h);
                    return this;
                }
                var notEmpty = function (model) {
                    if (model !== undefined && model !== "" && model !== null) {
                        return true;
                    } else {
                        return false
                    }
                }
            }]
        }
    }
    theDirective.$inject = [];
    module.directive(directiveName, theDirective);

})(null, window._);;angular.module('cwt.date-picker').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('templates/cwt-date-picker.html',
    "<p class=\"input-group cwt-date-picker\">\n" +
    "<input name=name ng-class=\"{'date-picker-has-error': ctrl.hasError}\" class=form-control uib-datepicker-popup={{ctrl.format}} data-ng-model=ctrl.displayModel is-open=ctrl.opened datepicker-options=ctrl.dateOptions alt-input-formats=ctrl.altInputFormats datepicker-template-url=templates/date-picker.tpl.html datepicker-popup-template-url=templates/date-picker-popup.tpl.html show-button-bar=true placeholder={{ctrl.placeholder}}>\n" +
    "<span class=input-group-btn>\n" +
    "<button type=button class=\"btn btn-primary\" data-ng-click=ctrl.open($event)>\n" +
    "<i class=\"glyphicon glyphicon-calendar\"></i>\n" +
    "</button>\n" +
    "</span>\n" +
    "</p>"
  );


  $templateCache.put('templates/date-picker-combined-day.tpl.html',
    "<table role=grid aria-labelledby={{::uniqueId}}-title aria-activedescendant={{activeDateId}} style=width:100%>\n" +
    "<thead>\n" +
    "<tr>\n" +
    "<th colspan=2><button type=button class=\"btn btn-default btn-sm pull-left uib-left\" ng-click=move(-12) tabindex=-1><i aria-hidden=true class=\"glyphicon glyphicon-chevron-left\"></i><span class=sr-only>previous</span></button></th>\n" +
    "<th colspan=2><button id={{::uniqueId}}-title role=heading aria-live=assertive aria-atomic=true type=button class=\"btn btn-default btn-sm uib-title\" ng-disabled=\"datepickerMode === maxMode\" tabindex=-1><strong>{{title | limitTo : 4 : title.length-4}}</strong></button></th>\n" +
    "<th colspan=2><button type=button class=\"btn btn-default btn-sm pull-right uib-right\" ng-click=move(12) tabindex=-1><i aria-hidden=true class=\"glyphicon glyphicon-chevron-right\"></i><span class=sr-only>next</span></button></th>\n" +
    "</tr>\n" +
    "</thead>\n" +
    "</table>\n" +
    "<table role=grid aria-labelledby={{::uniqueId}}-title aria-activedescendant={{activeDateId}} style=width:100%>\n" +
    "<thead>\n" +
    "<tr>\n" +
    "<th colspan=2><button type=button class=\"btn btn-default btn-sm pull-left uib-left\" ng-click=move(-1) tabindex=-1><i aria-hidden=true class=\"glyphicon glyphicon-chevron-left\"></i><span class=sr-only>previous</span></button></th>\n" +
    "<th colspan=2><button id={{::uniqueId}}-title role=heading aria-live=assertive aria-atomic=true type=button class=\"btn btn-default btn-sm uib-title\" ng-disabled=\"datepickerMode === maxMode\" tabindex=-1><strong>{{title | limitTo : title.length-4 : 0}}</strong></button></th>\n" +
    "<th colspan=2><button type=button class=\"btn btn-default btn-sm pull-right uib-right\" ng-click=move(1) tabindex=-1><i aria-hidden=true class=\"glyphicon glyphicon-chevron-right\"></i><span class=sr-only>next</span></button></th>\n" +
    "</tr>\n" +
    "</thead>\n" +
    "</table>\n" +
    "<table role=grid aria-labelledby={{::uniqueId}}-title aria-activedescendant={{activeDateId}}>\n" +
    "<thead>\n" +
    "<tr>\n" +
    "<th ng-if=showWeeks class=text-center></th>\n" +
    "<th ng-repeat=\"label in ::labels track by $index\" class=text-center><small aria-label={{::label.full}}>{{::label.abbr | limitTo:1}}</small></th>\n" +
    "</tr>\n" +
    "</thead>\n" +
    "<tbody>\n" +
    "<tr class=uib-weeks ng-repeat=\"row in rows track by $index\">\n" +
    "<td ng-if=showWeeks class=\"text-center h6\"><em>{{ weekNumbers[$index] }}</em></td>\n" +
    "<td ng-repeat=\"dt in row\" class=\"uib-day text-center\" role=gridcell id={{::dt.uid}} ng-class=::dt.customClass>\n" +
    "<button type=button class=\"btn btn-default btn-sm uib-daypicker-btn\" uib-is-class=\"\n" +
    "        'uib-daypicker-active' for activeDt,\n" +
    "              'uib-daypicker-selected' for selectedDt \n" +
    "              on dt\" ng-class=\"{'hide-date':dt.secondary}\" ng-click=select(dt.date) ng-disabled=::dt.disabled tabindex=-1><span>{{::dt.label}}</span></button>\n" +
    "</td>\n" +
    "</tr>\n" +
    "</tbody>\n" +
    "</table>\n"
  );


  $templateCache.put('templates/date-picker-combined-month.tpl.html',
    "<table role=grid aria-labelledby={{::uniqueId}}-title aria-activedescendant={{activeDateId}} ng-init=\"\">\n" +
    "<tbody>\n" +
    "<tr class=uib-months role=row>\n" +
    "<td ng-repeat=\"dt in rows[0].concat(rows[1])\" class=\"uib-month text-center\" role=gridcell id={{::dt.uid}} ng-class=::dt.customClass>\n" +
    "<ng-include src=\"'templates/date-picker-combined-month-button.tpl.html'\"></ng-include>\n" +
    "</td>\n" +
    "</tr>\n" +
    "<tr class=uib-months role=row>\n" +
    "<td ng-repeat=\"dt in rows[2].concat(rows[3])\" class=\"uib-month text-center\" role=gridcell id={{::dt.uid}} ng-class=::dt.customClass>\n" +
    "<ng-include src=\"'templates/date-picker-combined-month-button.tpl.html'\"></ng-include>\n" +
    "</td>\n" +
    "</tr>\n" +
    "</tbody>\n" +
    "</table>\n" +
    "<hr>"
  );


  $templateCache.put('templates/date-picker-day.tpl.html',
    "<table role=grid aria-labelledby={{::uniqueId}}-title aria-activedescendant={{activeDateId}}>\n" +
    "<thead>\n" +
    "<tr>\n" +
    "<th><button type=button class=\"pull-left uib-left\" ng-click=move(-1) tabindex=-1><i aria-hidden=true class=\"glyphicon glyphicon-chevron-left\"></i><span class=sr-only>previous</span></button></th>\n" +
    "<th colspan=\"{{::5 + showWeeks}}\"><button id={{::uniqueId}}-title role=heading aria-live=assertive aria-atomic=true type=button class=uib-title ng-click=toggleMode() ng-disabled=\"datepickerMode === maxMode\" tabindex=-1><strong>{{title}}</strong></button></th>\n" +
    "<th><button type=button class=\"pull-right uib-right\" ng-click=move(1) tabindex=-1><i aria-hidden=true class=\"glyphicon glyphicon-chevron-right\"></i><span class=sr-only>next</span></button></th>\n" +
    "</tr>\n" +
    "<tr>\n" +
    "<th ng-if=showWeeks class=text-center></th>\n" +
    "<th ng-repeat=\"label in ::labels track by $index\" class=text-center><small aria-label={{::label.full}}>{{::label.abbr | limitTo:1}}</small></th>\n" +
    "</tr>\n" +
    "</thead>\n" +
    "<tbody>\n" +
    "<tr class=uib-weeks ng-repeat=\"row in rows track by $index\">\n" +
    "<td ng-if=showWeeks class=\"text-center h6\"><em>{{ weekNumbers[$index] }}</em></td>\n" +
    "<td ng-repeat=\"dt in row\" class=\"uib-day text-center\" role=gridcell id={{::dt.uid}} ng-class=::dt.customClass>\n" +
    "<button type=button class=\"btn btn-default btn-sm uib-daypicker-btn\" uib-is-class=\"\n" +
    "				'uib-daypicker-active' for activeDt,\n" +
    "            	'uib-daypicker-selected' for selectedDt	\n" +
    "            	on dt\" ng-class=\"{'hide-date':dt.secondary}\" ng-click=select(dt.date) ng-disabled=::dt.disabled tabindex=-1><span>{{::dt.label}}</span></button>\n" +
    "</td>\n" +
    "</tr>\n" +
    "</tbody>\n" +
    "</table>"
  );


  $templateCache.put('templates/date-picker-popup.tpl.html',
    "<ul role=presentation class=\"uib-datepicker-popup dropdown-menu uib-position-measure\" dropdown-nested ng-if=isOpen ng-keydown=keydown($event) ng-click=$event.stopPropagation()>\n" +
    "<li ng-transclude></li>\n" +
    "<li ng-if=showButtonBar class=uib-button-bar>\n" +
    "<button type=button class=\"btn btn-sm btn-primary uib-datepicker-current\" style=width:100% ng-click=\"select('today', $event)\" ng-disabled=\"isDisabled('today')\">{{ getText('current') }}</button>\n" +
    "<button type=button class=\"btn btn-sm btn-danger uib-clear\" style=width:100% ng-click=\"select(null, $event)\">{{ getText('clear') }}</button>\n" +
    "</li>\n" +
    "</ul>"
  );


  $templateCache.put('templates/date-picker.tpl.html',
    "<div ng-switch=datepickerMode>\n" +
    "<div uib-daypicker template-url=templates/date-picker-combined-day.tpl.html ng-switch-when=day tabindex=0 class=uib-daypicker></div>\n" +
    "<div uib-monthpicker ng-switch-when=month tabindex=0 class=uib-monthpicker></div>\n" +
    "<div uib-yearpicker ng-switch-when=year tabindex=0 class=uib-yearpicker></div>\n" +
    "</div>"
  );

}]);
