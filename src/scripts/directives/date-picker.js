'use strict';
(function (module, _) {
    try {
        module = angular.module('cwt.date-picker');
    } catch (e) {
        module = angular.module('cwt.date-picker', []);
    }
    var directiveName = "cwtDatePicker";
    var theDirective = function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                datePickerModel: '=',
                hasError: '=?',
                placeholder: '@',
                datePickerOutputModel: '=?'
            },
            templateUrl: 'templates/cwt-date-picker.html',
            controllerAs: 'ctrl',
            bindToController: true,
            controller: ['$scope', function ($scope) {
                var ctrl = this
                ctrl.format = 'dd MMM yyyy';
                ctrl.opened = false;
                ctrl.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 1,
                    initDate: null,
                    showWeeks: false
                };
                ctrl.altInputFormats = ['M!/d!/yyyy'];
                ctrl.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    ctrl.opened = !ctrl.opened;
                };
                var internalChange = false;
                $scope.$watch('ctrl.datePickerModel', function () {
                    var model = angular.copy(ctrl.datePickerModel);
                    if (!(model instanceof Date) && notEmpty(model)) {
                        if (typeof model === 'string') {
                            model = new Date(model.split('+')[0]);
                        }
                        else {
                            model = new Date(model);
                        }
                        if( model.getHours() != 0) {
                            model = model.addHours((model.getTimezoneOffset() / 60));
                        }
                        model.setHours(12, 0, 0, 0);
                        ctrl.displayModel = angular.copy(model);
                        model = model.getTime();
                    }
                    
                    if (notEmpty(model)) {
                        ctrl.displayModel = angular.copy(model);
                        if ((model instanceof Date)) {
                            if( model.getHours() != 0) {
                                model = model.addHours((model.getTimezoneOffset() / 60));
                            }
                            model.setHours(12, 0, 0, 0);
                            model = model.getTime();
                        }
                        
                    }
                    else {
                        ctrl.displayModel = model;
                    }
                    if (!internalChange) {
                        if (notEmpty(model)) {
                            if ((model instanceof Date)) {
                                if( model.getHours() != 0) {
                                    model = model.addHours((model.getTimezoneOffset() / 60));
                                }
                                model.setHours(12, 0, 0, 0);
                                model = model.getTime();
                            }
                        }
                        ctrl.datePickerOutputModel = model;
                    }
                    internalChange = true;

                }, true);

                $scope.$watch('ctrl.displayModel', function () {
                    if (!internalChange) {
                        if (ctrl.displayModel) {
                            var model = angular.copy(ctrl.displayModel)
                            if ((model instanceof Date)) {
                                if( model.getHours() != 0) {
                                    model = model.addHours((model.getTimezoneOffset() / 60));
                                }
                                model.setHours(12, 0, 0, 0);
                            }
                            ctrl.datePickerOutputModel = model.getTime();
                            ctrl.datePickerModel = ctrl.displayModel;
                            internalChange = true;
                        }
                        else {
                            ctrl.datePickerOutputModel = null;
                        }
                    } else {
                        internalChange = false;
                    }
                });

                Date.prototype.addHours = function (h) {
                    this.setHours(this.getHours() + h);
                    return this;
                }
                var notEmpty = function (model) {
                    if (model !== undefined && model !== "" && model !== null) {
                        return true;
                    } else {
                        return false
                    }
                }
            }]
        }
    }
    theDirective.$inject = [];
    module.directive(directiveName, theDirective);

})(null, window._);